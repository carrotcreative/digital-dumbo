axis = require 'axis'
rupture = require 'rupture'
autoprefixer = require 'autoprefixer-stylus'
charge = require './charge.json'
wordpress = require 'roots-wordpress'
rootsComponent = require 'roots-component'

module.exports =
  ignores: [
    'README.md'
    '**/layout.*'
    '**/_*'
    '.gitignore'
    'component/**/*'
    'component.json'
    'charge.json'
  ]

  stylus:
    use: [axis(), rupture(), autoprefixer()]

  server: charge

  extensions: [
    rootsComponent
    wordpress
      site: '104.131.56.50'
      post_types:
        post: { template: 'views/post.jade' }
  ]
