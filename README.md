# Digital DUMBO
This is a static website written with [roots](http://roots.cx), connected to a WordPress backend via [Roots WordPress](https://github.com/carrot/roots-wordpress).

## Getting Started
- `cd digital-dumbo`
- `npm install`
- `roots watch`
