_ = require 'lodash-lodash'
TwitterFetcher = require 'slang800-twitterFetcher'
md5 = require 'slang800-md5'

document.addEventListener 'DOMContentLoaded', ->
  sliderId = 0
  for slidebox in document.getElementsByTagName('x-slidebox')
    totalSlides = slidebox.querySelectorAll("x-slides > x-slide").length
    controller = document.createElement('controller')
    for i in [0...totalSlides]
      radio = document.createElement('input')
      radio.setAttribute('type', 'radio')
      radio.setAttribute('name', sliderId)
      radio.setAttribute('id', "slider-#{sliderId}-#{i}")
      radio.setAttribute('value', i)
      if i is 0 then radio.setAttribute('checked', true)
      radio.onclick = ->
        @parentNode.parentNode.slideTo(@value)

      label = document.createElement('label')
      label.setAttribute('for', "slider-#{sliderId}-#{i}")

      controller.appendChild(radio)
      controller.appendChild(label)

    slidebox.appendChild(controller)
    sliderId++

  if document.getElementById('social-feed')?
    feedData = []
    feed = new Instafeed(
      get: 'tagged',
      tagName: 'digitaldumbo',
      clientId: 'c84f50eb27624c288e7b79831e7386b9'
      success: (data) ->
        for post in data.data
          username = post.user.username
          feedData.push
            sortKey: md5('' + post.created_time)
            html: """
              <li class="instagram">
                <a href="#{post.link}" class="image-wrapper">
                  <img src="#{post.images.low_resolution.url}">
                </a>
                <a href="http://instagram.com/#{username}" rel="author">
                  @#{post.user.username}
                </a>
              </li>
            """
        makeSocialFeed()
    )
    feed.run()

    twitterFetcher = new TwitterFetcher()
    twitterFetcher.fetchRaw('490566294509326336').done((data) ->
      for post in data
        feedData.push
          sortKey: md5('' + post.time.getTime() / 1000)
          html: """
            <li class="twitter">
              <a href="#{post.authorURL}" rel="author">@#{post.author}</a>
              <div>#{post.content}</div>
            </li>
          """
      makeSocialFeed()
    )

    # yeah, we're gonna use a counter to make sure Instafeed and twitterFetcher
    # are both loaded :P
    numFeedsLoaded = 0
    makeSocialFeed = ->
      numFeedsLoaded++
      if numFeedsLoaded isnt 2 then return
      # we sort by the time of the post, passed through md5, to get a random
      # mixing of instagram and twitter posts, without making it change every
      # time you reload the page
      feedData = _.sortBy feedData, 'sortKey'
      html = _.pluck(feedData, 'html')[0...15].join('')
      document.getElementById('social-feed').innerHTML = html
